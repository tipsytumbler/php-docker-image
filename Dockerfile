FROM php:7.2-apache

ARG BUILD_DATE
ARG VCS_REF
ENV COMPOSER_ALLOW_SUPERUSER 1


LABEL   Maintainer="Zaher Ghaibeh <zaher@triptap.it>" \
        Description="PHP & Apache container based on debian stretch-slim linux destrobution." \
        org.label-schema.name="triptap/php:latest" \
        org.label-schema.description="PHP & Apache container based on debian stretch-slim linux destrobution." \
        org.label-schema.build-date=$BUILD_DATE \
        org.label-schema.vcs-url="https://bitbucket.org/tipsytumbler/php-docker-image.git" \
        org.label-schema.vcs-ref=$VCS_REF \
        org.label-schema.schema-version="1.0.0"

WORKDIR /var/www/html/

RUN apt-get update && apt-get install -y \
    git mysql-client curl openssh-client libxml2-dev libpng-dev \
    && docker-php-source extract \
    && pecl install redis \
    && docker-php-ext-enable redis \
    && docker-php-source delete \
    && docker-php-ext-install pdo_mysql intl zip gd mysqli calendar gettext intl opcache xml zip \
    && echo "memory_limit = 128M" >> /usr/local/etc/php/php.ini \
    && echo "post_max_size = 100M" >> /usr/local/etc/php/php.ini \
    && echo "upload_max_filesize = 200M" >> /usr/local/etc/php/php.ini \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && sed -i 's|AllowOverride None|AllowOverride All|' /etc/apache2/apache2.conf \
    && a2enmod rewrite && service apache2 restart \
    && apt-get autoremove -y && apt-get autoclean -y && rm -rf /var/lib/apt/lists/*

HEALTHCHECK --interval=5m --timeout=3s --retries=3 --start-period=3s CMD curl -f http://localhost/ || exit 1